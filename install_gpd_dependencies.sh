#! /bin/sh
#GPD SETUP

#Download and install dependencies for GPD outside ws
cd
wget https://raw.githubusercontent.com/PickNikRobotics/deep_grasp_demo/master/pcl_install.sh
chmod +x pcl_install.sh
sudo ./pcl_install.sh
wget https://raw.githubusercontent.com/PickNikRobotics/deep_grasp_demo/master/opencv_install.sh
chmod +x opencv_install.sh
sudo ./opencv_install.sh

#Download and install GPD
git clone https://github.com/atenpas/gpd
cd ~/ws_grasp_baselines/src/moveit_task_constructor_gpd/gpd
mkdir build && cd build
cmake ..
make -j
sudo make install

#Download and install moveit task constructor
cd ~/ws_grasp_baselines/src
git clone https://github.com/ros-planning/moveit_task_constructor.git
git clone https://ardonpaola@bitbucket.org/ardonp/deep_grasp_task.git
cd moveit_task_constructor
rosdep install --from-paths . --ignore-src --rosdistro $ROS_DISTRO
cd ~/ws_grasp_baselines/
catkin build
