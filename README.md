# GDP SETUP
Demo shows how to use the Grasp Pose Detection [GPD](https://github.com/atenpas/gpd) library with the MoveIt Task Constructor for a pick and place task. This repository is adapted from the [MoveIt Task Constructor](https://github.com/PickNikRobotics/deep_grasp_demo/tree/master/moveit_task_constructor_gpd). GPD samples grasp candidates within a point cloud and returns a list of the candidates and their costs.

## Prerequisites
Install MoveIt Task Constructor to be able to pick-and-place
```
cd ~/ws_grasp_baselines/src
git clone  https://ardonpaola@bitbucket.org/ardonp/moveit_task_constructor_gpd.git
cd moveit_task_constructor_gpd
bash install_gpd_dependencies.sh
```

## Installation
- In `moveit_task_constructor_gpd/config/gpd_congfig.yaml` navigate to line 33 and update `weights_file` to contain the absolute file path to the location of the lenet params directory. This directory contains the learned model weights and is located inside `~/ws_grasp_baselines/src/moveit_task_constructor_gpd/gpd` folder.

- Build the package
```
cd ~/ws_grasp_baselines
catkin build
echo 'source ~/ws_grasp_baselines/devel/setup.bash' >> ~/.bashrc
```

## Running
### Using Fake Controllers
- You don't need Gazebo for this one. The point cloud data for the cylinder was collected ahead of time and located in `data/pointclouds/cylinder.pcd`

- To run the pick and place demo:
```
roslaunch moveit_task_constructor_gpd <your_robot>_gpd_demo.launch
```

- To see the generated grasps with their cost (generated at `~/moveit_task_constructor_gpd/src/grasp_detection.cpp` and remember to have a `roscore` open in another terminal)
```
rostopic echo /generate_grasps/feedback
```

## Nodes
### grasp_cloud_detection
This node uses GPD to sample grasp candidates from a point cloud. Communication with the MoveIt Task Constructor is achieved using ROS action messages. The action client sends the grasp candidates along with the costs back to the `DeepGraspPose` stage. The point cloud can either be loaded from a file or the node can subscribe to the point cloud topic name of type `(sensor_msgs/PointCloud2)` specified as an input parameter. If subscribing to a point cloud topic, the table plane is removed and the resulting point cloud is published on `segmented_cloud`.

### point_cloud_server
This node is primarily used for collecting point cloud data. The `save_point_cloud` service is offered to save point clouds to the user specified file. The node subscribes to the point cloud topic name of type `(sensor_msgs/PointCloud2)` specified as an input parameter. This node also offers the functionality to keep points within specified cartesian lower and upper bounds. Additionally, the table plane can be removed from the point cloud specified by the `remove_table` parameter. The resulting point cloud is published on `filtered_cloud`.

To save the filtered point cloud: `rosservice call /save_point_cloud "cloud_file:'my_cloud_file.pcd'"`

## Tips
If you are processing the point clouds on your own, it is recommended to remove the table plane. GPD will find grasp candidates that try to pick up the object by the table plane. Sometimes segmentation is not enough and GPD tries to grasp other pieces of a point cloud that are not the object. Other portions of the point cloud can be removed using PCL's `filter()` function. This allows you to specify cartesian limits on which points to keep. See the `removeTable()` and `passThroughFilter()` functions in `cloud_utils.cpp.`
